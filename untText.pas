unit untText;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OleCtrls, SHDocVw, StdCtrls, jpeg, ExtCtrls, General;

type
  TfraText = class(TFrame)
    WebBrowser1: TWebBrowser;
    Image1: TImage;
    ScrollBox1: TScrollBox;
    ScrollBox2: TScrollBox;
    Image3: TImage;
    Image4: TImage;
    Image5: TImage;
    Image6: TImage;
    Image7: TImage;
    Image8: TImage;
    Image9: TImage;
    Image2: TImage;
    Image10: TImage;
    Image11: TImage;
    Image12: TImage;
    Image13: TImage;
    Image14: TImage;
    Image15: TImage;
    Image16: TImage;
    Image17: TImage;
    Image18: TImage;
    Image19: TImage;
    Image20: TImage;
    Image21: TImage;
    Image22: TImage;
    Image23: TImage;
    Image24: TImage;
    Image25: TImage;
    Image26: TImage;
    Image27: TImage;
    Image28: TImage;
    Image29: TImage;
    Image30: TImage;
    Image31: TImage;
    Image32: TImage;
    Image33: TImage;
    Image34: TImage;
    Image35: TImage;
    Image36: TImage;
    Image37: TImage;
    Image38: TImage;
    Image39: TImage;
    procedure Image6Click(Sender: TObject);
    procedure Image7Click(Sender: TObject);
    procedure Image8Click(Sender: TObject);
    procedure Image9Click(Sender: TObject);
    procedure Image10Click(Sender: TObject);
    procedure Image11Click(Sender: TObject);
    procedure Image12Click(Sender: TObject);
    procedure Image13Click(Sender: TObject);
    procedure Image14Click(Sender: TObject);
    procedure Image15Click(Sender: TObject);
    procedure Image16Click(Sender: TObject);
    procedure Image17Click(Sender: TObject);
    procedure Image18Click(Sender: TObject);
    procedure Image19Click(Sender: TObject);
    procedure Image20Click(Sender: TObject);
    procedure Image21Click(Sender: TObject);
    procedure Image26Click(Sender: TObject);
    procedure Image25Click(Sender: TObject);
    procedure Image24Click(Sender: TObject);
    procedure Image23Click(Sender: TObject);
    procedure Image22Click(Sender: TObject);
    procedure Image31Click(Sender: TObject);
    procedure Image30Click(Sender: TObject);
    procedure Image29Click(Sender: TObject);
    procedure Image28Click(Sender: TObject);
    procedure Image27Click(Sender: TObject);
    procedure Image39Click(Sender: TObject);
    procedure Image38Click(Sender: TObject);
    procedure Image37Click(Sender: TObject);
    procedure Image36Click(Sender: TObject);
    procedure Image35Click(Sender: TObject);
    procedure Image34Click(Sender: TObject);
    procedure Image33Click(Sender: TObject);
    procedure Image32Click(Sender: TObject);
    procedure ScrollBox1ConstrainedResize(Sender: TObject; var MinWidth,
      MinHeight, MaxWidth, MaxHeight: Integer);
  private
    procedure LoadPage(No, AB: Byte);
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

uses untMain;

{$R *.dfm}

procedure TfraText.LoadPage(No, AB: Byte);
begin
  if AB = 1 then
    WebBrowser1.Navigate(TextPathA + IntToStr(No) + '.htm')
  else if AB = 2 then
    WebBrowser1.Navigate(TextPathB + IntToStr(No) + '.htm');
  WebBrowser1.SetFocus;
end;

procedure TfraText.Image6Click(Sender: TObject);
begin
  LoadPage(1, 1);
end;

procedure TfraText.Image7Click(Sender: TObject);
begin
  LoadPage(2, 1);
end;

procedure TfraText.Image8Click(Sender: TObject);
begin
  LoadPage(3, 1);
end;

procedure TfraText.Image9Click(Sender: TObject);
begin
  LoadPage(4, 1);
end;

procedure TfraText.Image10Click(Sender: TObject);
begin
  LoadPage(1, 2);
end;

procedure TfraText.Image11Click(Sender: TObject);
begin
  LoadPage(2, 2);
end;

procedure TfraText.Image12Click(Sender: TObject);
begin
  LoadPage(3, 2);
end;

procedure TfraText.Image13Click(Sender: TObject);
begin
  LoadPage(4, 2);
end;

procedure TfraText.Image14Click(Sender: TObject);
begin
  LoadPage(5, 2);
end;

procedure TfraText.Image15Click(Sender: TObject);
begin
  LoadPage(6, 2);
end;

procedure TfraText.Image16Click(Sender: TObject);
begin
  LoadPage(7, 2);
end;

procedure TfraText.Image17Click(Sender: TObject);
begin
  LoadPage(8, 2);
end;

procedure TfraText.Image18Click(Sender: TObject);
begin
  LoadPage(9, 2);
end;

procedure TfraText.Image19Click(Sender: TObject);
begin
  LoadPage(10, 2);
end;

procedure TfraText.Image20Click(Sender: TObject);
begin
  LoadPage(11, 2);
end;

procedure TfraText.Image21Click(Sender: TObject);
begin
  LoadPage(12, 2);
end;

procedure TfraText.Image26Click(Sender: TObject);
begin
  LoadPage(13, 2);
end;

procedure TfraText.Image25Click(Sender: TObject);
begin
  LoadPage(14, 2);
end;

procedure TfraText.Image24Click(Sender: TObject);
begin
  LoadPage(15, 2);
end;

procedure TfraText.Image23Click(Sender: TObject);
begin
  LoadPage(16, 2);
end;

procedure TfraText.Image22Click(Sender: TObject);
begin
  LoadPage(17, 2);
end;

procedure TfraText.Image31Click(Sender: TObject);
begin
  LoadPage(18, 2);
end;

procedure TfraText.Image30Click(Sender: TObject);
begin
  LoadPage(19, 2);
end;

procedure TfraText.Image29Click(Sender: TObject);
begin
  LoadPage(20, 2);
end;

procedure TfraText.Image28Click(Sender: TObject);
begin
  LoadPage(21, 2);
end;

procedure TfraText.Image27Click(Sender: TObject);
begin
  LoadPage(22, 2);
end;

procedure TfraText.Image39Click(Sender: TObject);
begin
  LoadPage(23, 2);
end;

procedure TfraText.Image38Click(Sender: TObject);
begin
  LoadPage(24, 2);
end;

procedure TfraText.Image37Click(Sender: TObject);
begin
  LoadPage(25, 2);
end;

procedure TfraText.Image36Click(Sender: TObject);
begin
  LoadPage(26, 2);
end;

procedure TfraText.Image35Click(Sender: TObject);
begin
  LoadPage(27, 2);
end;

procedure TfraText.Image34Click(Sender: TObject);
begin
  LoadPage(28, 2);
end;

procedure TfraText.Image33Click(Sender: TObject);
begin
  LoadPage(29, 2);
end;

procedure TfraText.Image32Click(Sender: TObject);
begin
  LoadPage(30, 2);
end;

procedure TfraText.ScrollBox1ConstrainedResize(Sender: TObject;
  var MinWidth, MinHeight, MaxWidth, MaxHeight: Integer);
begin
  Image2.Top := 0;
end;

end.
