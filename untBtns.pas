unit untBtns;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, jpeg, ExtCtrls, untPlayer;

type
  TfraBtns = class(TFrame)
    Image1: TImage;
    img1: TImage;
    lbl1: TLabel;
    img2: TImage;
    lbl2: TLabel;
    img3: TImage;
    lbl3: TLabel;
    img4: TImage;
    lbl4: TLabel;
    img5: TImage;
    lbl5: TLabel;
    procedure lbl1Click(Sender: TObject);
    procedure lbl2Click(Sender: TObject);
    procedure lbl3Click(Sender: TObject);
    procedure lbl1MouseEnter(Sender: TObject);
    procedure lbl1MouseLeave(Sender: TObject);
    procedure lbl4MouseEnter(Sender: TObject);
    procedure lbl2MouseEnter(Sender: TObject);
    procedure lbl3MouseEnter(Sender: TObject);
    procedure lbl4Click(Sender: TObject);
    procedure lbl5Click(Sender: TObject);
    procedure lbl5MouseEnter(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Imaging1;
    procedure Imaging(No: Byte);
  end;

implementation

uses
  untMain;

{$R *.dfm}

procedure TfraBtns.Imaging1;//(No: Integer);
begin
  img1.Visible := False;
  img2.Visible := False;
  img3.Visible := False;
  img4.Visible := False;
  img5.Visible := False;
  case NowPlay of
    1: img1.Visible := True;
    2: img2.Visible := True;
    3: img3.Visible := True;
    4: img4.Visible := True;
    5: img5.Visible := True;
  end;
end;

procedure TfraBtns.lbl1Click(Sender: TObject);
begin
  Imaging(1);
end;

procedure TfraBtns.lbl2Click(Sender: TObject);
begin
  Imaging(2);
end;

procedure TfraBtns.lbl3Click(Sender: TObject);
begin
  Imaging(3);
end;

procedure TfraBtns.lbl1MouseEnter(Sender: TObject);
begin
  img1.Visible := True;
end;

procedure TfraBtns.lbl1MouseLeave(Sender: TObject);
begin
  Imaging(0);
end;

procedure TfraBtns.lbl4MouseEnter(Sender: TObject);
begin
  img4.Visible := True;
end;

procedure TfraBtns.lbl2MouseEnter(Sender: TObject);
begin
  img2.Visible := True;
end;

procedure TfraBtns.lbl3MouseEnter(Sender: TObject);
begin
  img3.Visible := True;
end;

procedure TfraBtns.Imaging(No: Byte);
begin
  if No <> 0 then
  begin
    case FunctionType of
      ftVideo: frmMain.fraVideo1.fraPlayer1.PlayFile(No);
      ftAudio: frmMain.fraAudio1.fraPlayer1.PlayFile(No);
      ftClip : frmMain.fraClip1.fraPlayer1.PlayFile(No);
    end;
  end;
  Imaging1;//(No);
end;

procedure TfraBtns.lbl4Click(Sender: TObject);
begin
  Imaging(4);
end;

procedure TfraBtns.lbl5Click(Sender: TObject);
begin
  Imaging(5);
end;

procedure TfraBtns.lbl5MouseEnter(Sender: TObject);
begin
  img5.Visible := True;
end;

end.
