unit untSplash;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, jpeg, General;

type
  TfrmSplash = class(TForm)
    imgSplash: TImage;
    Timer1: TTimer;
    tmrBlend: TTimer;
    procedure imgSplashClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure Timer1Timer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure tmrBlendTimer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSplash: TfrmSplash;

implementation

{$R *.dfm}

procedure TfrmSplash.imgSplashClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmSplash.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if (Key in [#32, #27, #13]) then
    Close;
end;

procedure TfrmSplash.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  Close;
end;

procedure TfrmSplash.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmSplash.tmrBlendTimer(Sender: TObject);
var
  ab: Integer;
begin
  if Tag = 0 then
  begin
    ab := AlphaBlendValue + ALPHABLEND_DIFF;
    if ab > 255 then
    begin
      ab := 255;
      AlphaBlendValue := ab;
      AlphaBlend := False;
      tmrBlend.Enabled := False;
      Timer1.Enabled := True;
    end
    else
      AlphaBlendValue := ab;
  end
  else if Tag = 1 then
  begin
    ab := AlphaBlendValue - ALPHABLEND_DIFF;
    if ab < 0 then
    begin
      ab := 0;
      AlphaBlendValue := ab;
      tmrBlend.Enabled := False;
      Close;
    end
    else
      AlphaBlendValue := ab;
  end
end;

procedure TfrmSplash.FormCreate(Sender: TObject);
begin
  Tag := 0;
  ImageLoad(imgSplash, 'imgSplash.jpg');
end;

procedure TfrmSplash.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if Tag = 0 then
  begin
    Tag := 1;
    CanClose := False;
    AlphaBlend := True;
    tmrBlend.Enabled := True;
  end
  else
    CanClose := True;
end;

procedure TfrmSplash.FormShow(Sender: TObject);
begin
  ReadOrCreateIni('Splash');
end;

end.
