unit untClip;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, StdCtrls, MPlayer, jpeg, ExtCtrls, untPlayer, ActnList;

type
  TfraClip = class(TFrame)
    Image1: TImage;
    pnlScreen: TPanel;
    imgDisplayPanel: TImage;
    fraPlayer1: TfraPlayer;
    procedure imgDisplayPanelDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

uses untMain;

{$R *.dfm}

procedure TfraClip.imgDisplayPanelDblClick(Sender: TObject);
begin
  fraPlayer1.actFullScreen.OnExecute(Sender);
end;

end.
