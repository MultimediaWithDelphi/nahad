unit untPlayer;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, jpeg, ExtCtrls, MPlayer, ActnList, MyVolControl,
  General;

type
  TfraPlayer = class(TFrame)
    imgPlayer: TImage;
    imgStop: TImage;
    imgRewind: TImage;
    imgRepeat: TImage;
    imgPlay: TImage;
    imgPause: TImage;
    imgNext: TImage;
    imgLoop: TImage;
    imgForward: TImage;
    lblFullTime: TLabel;
    imgPauseO: TImage;
    imgRwD: TImage;
    lblRewind: TLabel;
    imgStopD: TImage;
    lblStop: TLabel;
    imgFwD: TImage;
    imgNextD: TImage;
    lblNext: TLabel;
    imgRepD: TImage;
    lblRepeat: TLabel;
    imgLoopD: TImage;
    lblLoop: TLabel;
    imgPauseD: TImage;
    imgPlayD: TImage;
    lblPlayPause: TLabel;
    imgSound: TImage;
    lblChangeSound: TLabel;
    lblForward: TLabel;
    lblTime: TLabel;
    imgMute: TImage;
    imgMuteD: TImage;
    lblMute: TLabel;
    lblTrackUser: TLabel;
    Timer1: TTimer;
    ActionList1: TActionList;
    actPlayPause: TAction;
    actStop: TAction;
    actRewind: TAction;
    actForward: TAction;
    actPrevious: TAction;
    actNext: TAction;
    actVolHigh: TAction;
    actVolLow: TAction;
    actMute: TAction;
    actFullScreen: TAction;
    imgPreD: TImage;
    imgPre: TImage;
    lblPre: TLabel;
    imgTrackMove: TImage;
    mplPlayer: TMediaPlayer;
    procedure lblLoopClick(Sender: TObject);
    procedure lblRepeatClick(Sender: TObject);
    procedure lblStopClick(Sender: TObject);
    procedure lblPlayPauseClick(Sender: TObject);
    procedure lblPlayPauseMouseEnter(Sender: TObject);
    procedure lblPlayPauseMouseLeave(Sender: TObject);
    procedure lblStopMouseEnter(Sender: TObject);
    procedure lblStopMouseLeave(Sender: TObject);
    procedure lblRepeatMouseEnter(Sender: TObject);
    procedure lblRepeatMouseLeave(Sender: TObject);
    procedure lblLoopMouseEnter(Sender: TObject);
    procedure lblLoopMouseLeave(Sender: TObject);
    procedure lblTrackUserMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblForwardClick(Sender: TObject);
    procedure lblRewindClick(Sender: TObject);
    procedure lblNextClick(Sender: TObject);
    procedure lblPreClick(Sender: TObject);
    procedure actVolHighExecute(Sender: TObject);
    procedure actVolLowExecute(Sender: TObject);
    procedure lblRewindMouseEnter(Sender: TObject);
    procedure lblRewindMouseLeave(Sender: TObject);
    procedure lblPreMouseEnter(Sender: TObject);
    procedure lblPreMouseLeave(Sender: TObject);
    procedure lblForwardMouseEnter(Sender: TObject);
    procedure lblForwardMouseLeave(Sender: TObject);
    procedure lblNextMouseEnter(Sender: TObject);
    procedure lblNextMouseLeave(Sender: TObject);
    procedure lblChangeSoundMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblPreMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblPreMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblRewindMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblRewindMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblStopMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblStopMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblForwardMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblForwardMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblNextMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblNextMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblRepeatMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblRepeatMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblLoopMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblLoopMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblPlayPauseMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblPlayPauseMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure actFullScreenExecute(Sender: TObject);
    procedure actMuteExecute(Sender: TObject);
    procedure lblMuteClick(Sender: TObject);
    procedure lblMuteMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblMuteMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblMuteMouseEnter(Sender: TObject);
    procedure lblMuteMouseLeave(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure mplPlayerNotify(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure UpdateCurrTime(Position, Len: Int64);
    procedure PlayFile(FileNo: Byte);
    function UpdateFileName(n: Byte): String;
    procedure SoundHeightShow;
    procedure ChangeSound(v: Smallint);
  end;

function MiliSecondToTime(Sec: Int64): AnsiString;
procedure SetMute;
procedure ResetMute;

implementation

uses
  untMain, untBtns, untFullScreen;

{$R *.dfm}

procedure TfraPlayer.lblLoopClick(Sender: TObject);
begin
  Loop := not Loop;
  if Loop then
  begin
    Rep := False;
    imgRepeat.Visible := False;
  end;
end;

procedure TfraPlayer.lblRepeatClick(Sender: TObject);
begin
  Rep := not Rep;
  if Rep then
  begin
    Loop := False;
    imgLoop.Visible := False;
  end;
end;

procedure TfraPlayer.SoundHeightShow;
begin
  imgSound.Width := (Vol * lblChangeSound.Width) div 1000;
end;

procedure TfraPlayer.lblStopClick(Sender: TObject);
begin
  mplPlayer.Notify := True;
  try
    Timer1.Enabled := False;
    mplPlayer.Rewind;
    mplPlayer.Stop;
    imgTrackMove.Width := 0;
  except
    ;
  end;
end;

procedure TfraPlayer.lblPlayPauseClick(Sender: TObject);
begin
  try
    mplPlayer.Notify := True;
    if NowPlay <> 0 then
    begin
      if imgPause.Visible then
        mplPlayer.Stop
      else
      begin
        Timer1.Enabled := True;
        mplPlayer.Play;
        mplPlayer.OnNotify(mplPlayer);
      end;
    end;
  except;end;
end;

procedure TfraPlayer.lblPlayPauseMouseEnter(Sender: TObject);
begin
  if (NowPlay <> 0) then
    if imgPause.Visible then
      imgPauseO.Visible := True
    else if not imgPause.Visible then
      imgPlay.Visible := True;
end;

procedure TfraPlayer.lblPlayPauseMouseLeave(Sender: TObject);
begin
  if imgPauseO.Visible then
    imgPauseO.Visible := False;
  if imgPlay.Visible then
    imgPlay.Visible := False;
end;

procedure TfraPlayer.lblStopMouseEnter(Sender: TObject);
begin
  if (NowPlay <> 0) then
    imgStop.Visible := True;
end;

procedure TfraPlayer.lblStopMouseLeave(Sender: TObject);
begin
  imgStop.Visible := False;
end;

procedure TfraPlayer.lblRepeatMouseEnter(Sender: TObject);
begin
  imgRepeat.Visible := True;
end;

procedure TfraPlayer.lblRepeatMouseLeave(Sender: TObject);
begin
  if not Rep then
    imgRepeat.Visible := False;
end;

procedure TfraPlayer.lblLoopMouseEnter(Sender: TObject);
begin
  imgLoop.Visible := True;
end;

procedure TfraPlayer.lblLoopMouseLeave(Sender: TObject);
begin
  if not Loop then
    imgLoop.Visible := False;
end;

procedure TfraPlayer.lblTrackUserMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  l, t: Int64; // Length
begin
  try
    l := mplPlayer.Length;// div 1000;
    t := X * l div lblTrackUser.Width;
    if not Timer1.Enabled then
    begin
      imgTrackMove.Width := X;
    end;
    mplPlayer.Position := t;
    mplPlayer.Notify := True;
    if imgPause.Visible then
    begin
      mplPlayer.Play;
      mplPlayer.Notify := True;
    end;
  except
    ;
  end;
end;

procedure TfraPlayer.lblForwardClick(Sender: TObject);
var
  Playing: Boolean;
begin
  if NowPlay <> 0 then
  begin
    if mplPlayer.Mode = mpPlaying then
      Playing := True
    else
      Playing := False;
    mplPlayer.Position := mplPlayer.Position + 10000;
    UpdateCurrTime(mplPlayer.Position, mplPlayer.Length);
    if Playing then
      mplPlayer.Play;
    mplPlayer.Notify := True;
  end;
end;

procedure TfraPlayer.lblRewindClick(Sender: TObject);
var
  Playing: Boolean;
begin
  if NowPlay <> 0 then
  begin
    if mplPlayer.Mode = mpPlaying then
      Playing := True
    else
      Playing := False;
    mplPlayer.Position := mplPlayer.Position - 10000;
    UpdateCurrTime(mplPlayer.Position, mplPlayer.Length);
    if Playing then
      mplPlayer.Play;
    mplPlayer.Notify := True;
  end;
end;

procedure TfraPlayer.lblNextClick(Sender: TObject);
begin
  try
    if NowPlay <> 0 then
    begin
      NowPlay := NowPlay + 1;
      if NowPlay > MaxNo then
      begin
        NowPlay := 1;
      end;
      if (mplPlayer.Mode in
        [mpPlaying, mpPaused]) then
        PlayFile(NowPlay)
      else
        UpdateFileName(NowPlay);
    end;
    mplPlayer.Notify := True;
  except;end;
end;

procedure TfraPlayer.lblPreClick(Sender: TObject);
begin
  try
    if NowPlay <> 0 then
    begin
      NowPlay := NowPlay - 1;
      if NowPlay < 1 then
      begin
        NowPlay := MaxNo;
      end;
      if (mplPlayer.Mode in
        [mpPlaying, mpPaused]) then
        PlayFile(NowPlay)
      else
        UpdateFileName(NowPlay);
    end;
    mplPlayer.Notify := True;
  except;end;
end;

procedure TfraPlayer.UpdateCurrTime(Position, Len: Int64);
var
  t: Int64; // Current Position, Length
begin
  try
    t := Position * lblTrackUser.Width div Len;
   // if not (t >= 316) then  // 317 is the length of lblTrackbar
     // t := (t div 5) * 5;
    imgTrackMove.Width := t;
//    imgTrackMove.Width := lblTrackUser.Width - t;
//    imgTrackMove.Left := lblTrackUser.Left + t;
    lblTime.Caption := MiliSecondToTime(Position);
  except
    ;
  end;
end;

procedure TfraPlayer.PlayFile(FileNo: Byte);
var
  t: TRect;
begin
  lblTrackUser.Visible := True;
  try
    if frmMain.mplBack.Mode = mpPlaying then
    begin
      frmMain.mplBack.Stop;
      mplBackPlay := False;
    end;
    frmMain.mplBack.Notify := True;
  except;end;
  UpdateFileName(FileNo);
  try
    mplPlayer.Play;
    mplPlayer.OnNotify(mplPlayer);
    Timer1.Enabled := True;
  except; end;
  if FunctionType in [ftVideo, ftClip] then
  begin
    with t do
    begin
      Left := 0;
      Top := 0;
      Right := mplPlayer.Display.Width;
      Bottom := mplPlayer.Display.Height;
    end;
    mplPlayer.DisplayRect := t;
  end;
end;

function TfraPlayer.UpdateFileName(n: Byte): String;
var
  fn: String;
begin
  case FunctionType of
    ftAudio: fn := AudioPath + IntToStr(n) + '.mp3';
    ftVideo: fn := VideoPath + IntToStr(n) + '.wmv';
    ftClip : fn := ClipPath + IntToStr(n) + '.wmv';
  end;
  if (fn <> mplPlayer.FileName) or
    (not(mplPlayer.Mode in [mpNotReady, mpOpen, mpPlaying,
     mpPaused, mpStopped, mpSeeking, mpRecording])) then
  begin
    try
      mplPlayer.Close;
    except
      ;
    end;
    imgTrackMove.Width := 0;
    try
      mplPlayer.FileName := fn;
      mplPlayer.Open;
      lblPlayPause.Enabled := True;
      lblFullTime.Caption := MiliSecondToTime(mplPlayer.Length);
      NowPlay := n;
      case FunctionType of
        ftVideo: frmMain.fraVideo1.fraBtns1.Imaging1;
        ftAudio: frmMain.fraAudio1.fraBtns1.Imaging1;
        //ftClip: frmMain.fraVideo1.fraBtns1.Imaging1;
      end;
    except;end;
  end;
end;

function MiliSecondToTime(Sec: Int64): AnsiString;
var
  h, m, s: Byte;
  TimeStr: AnsiString;
begin
  Sec := Sec div 1000;
  m := Sec div 60;
  s := Sec - (m * 60);
  h := 0;
  if m >= 60 then
  begin
    h := m div 60;
    m := m - (h * 60);
  end;
  TimeStr := '';
  if h <> 0 then
    if m < 10 then
      TimeStr := IntToStr(h) + ':0'
    else
      TimeStr := IntToStr(h) + ':';
  TimeStr := TimeStr + IntToStr(m) + ':';
  if s < 10 then
    TimeStr := TimeStr + '0';
  TimeStr := TimeStr + IntToStr(s);
  Result := TimeStr;
end;

procedure TfraPlayer.ChangeSound(v: Smallint);
begin
  if v < 0 then
    v := 0
  else if v > 1000 then
    v := 1000;
  Vol := v;
  SoundHeightShow;
  if not Mute then
    MPSetVolume(mplPlayer, Vol);
end;

procedure TfraPlayer.actVolHighExecute(Sender: TObject);
begin
  ChangeSound(Vol + 100);
end;

procedure TfraPlayer.actVolLowExecute(Sender: TObject);
begin
  ChangeSound(Vol - 100);
end;

procedure TfraPlayer.lblRewindMouseEnter(Sender: TObject);
begin
  imgRewind.Visible := True;
end;

procedure TfraPlayer.lblRewindMouseLeave(Sender: TObject);
begin
  imgRewind.Visible := False;
end;

procedure TfraPlayer.lblPreMouseEnter(Sender: TObject);
begin
  imgPre.Visible := True;
end;

procedure TfraPlayer.lblPreMouseLeave(Sender: TObject);
begin
  imgPre.Visible := False;
end;

procedure TfraPlayer.lblForwardMouseEnter(Sender: TObject);
begin
  imgForward.Visible := True;
end;

procedure TfraPlayer.lblForwardMouseLeave(Sender: TObject);
begin
  imgForward.Visible := False;
end;

procedure TfraPlayer.lblNextMouseEnter(Sender: TObject);
begin
  imgNext.Visible := True;
end;

procedure TfraPlayer.lblNextMouseLeave(Sender: TObject);
begin
  imgNext.Visible := False;
end;

procedure TfraPlayer.lblChangeSoundMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  l, t: Int64; // Length
begin
  try
    l := 1000;
    t := X * l div lblChangeSound.Width;
    ChangeSound(t);
  except
    ;
  end;
end;

procedure TfraPlayer.lblPreMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  imgPreD.Visible := True;
end;

procedure TfraPlayer.lblPreMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  imgPreD.Visible := False;
end;

procedure TfraPlayer.lblRewindMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  imgRwD.Visible := True;
end;

procedure TfraPlayer.lblRewindMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  imgRwD.Visible := False;
end;

procedure TfraPlayer.lblStopMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  imgStopD.Visible := True;
end;

procedure TfraPlayer.lblStopMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  imgStopD.Visible := False;
end;

procedure TfraPlayer.lblForwardMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  imgFwD.Visible := True;
end;

procedure TfraPlayer.lblForwardMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  imgFwD.Visible := False;
end;

procedure TfraPlayer.lblNextMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  imgNextD.Visible := True;
end;

procedure TfraPlayer.lblNextMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  imgNextD.Visible := False;
end;

procedure TfraPlayer.lblRepeatMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  imgRepD.Visible := True;
end;

procedure TfraPlayer.lblRepeatMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  imgRepD.Visible := False;
end;

procedure TfraPlayer.lblLoopMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  imgLoopD.Visible := True;
end;

procedure TfraPlayer.lblLoopMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  imgLoopD.Visible := False;
end;

procedure TfraPlayer.lblPlayPauseMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if (NowPlay <> 0) then
    if imgPause.Visible then
      imgPauseD.Visible := True
    else if not imgPause.Visible then
      imgPlayD.Visible := True;
end;

procedure TfraPlayer.lblPlayPauseMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    imgPauseD.Visible := False;
    imgPlayD.Visible := False;
end;

procedure TfraPlayer.actFullScreenExecute(Sender: TObject);
var
  t: TRect;
begin
  try
    frmFullScreen := TfrmFullScreen.Create(Self);
    mplPlayer.Display := frmFullScreen.Panel1;
    with t do
    begin
      Left := 0;
      Top := 0;
      Right := frmFullScreen.Panel1.Width;
      Bottom := frmFullScreen.Panel1.Height;
    end;
    mplPlayer.DisplayRect := t;
//    MediaPlayer1.Display.
    frmFullScreen.ShowModal;
  except;end;
end;

procedure TfraPlayer.actMuteExecute(Sender: TObject);
begin
  if not Mute then
  begin
    MPSetVolume(mplPlayer, 0);
    imgMute.Visible := True;
    SetMute;
  end
  else
  begin
    MPSetVolume(mplPlayer, Vol);
    SoundHeightShow;
    imgMute.Visible := False;
    ResetMute;
  end;
end;

procedure TfraPlayer.lblMuteClick(Sender: TObject);
begin
  actMute.OnExecute(Sender);
end;

procedure TfraPlayer.lblMuteMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  imgMuteD.Visible := True;
end;

procedure TfraPlayer.lblMuteMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  imgMuteD.Visible := False;
end;

procedure TfraPlayer.lblMuteMouseEnter(Sender: TObject);
begin
  imgMute.Visible := True;
end;

procedure TfraPlayer.lblMuteMouseLeave(Sender: TObject);
begin
  if not Mute then
    imgMute.Visible := False;
end;

procedure SetMute;
begin
  Mute := True;
end;

procedure ResetMute;
begin
  Mute := False;
end;

procedure TfraPlayer.Timer1Timer(Sender: TObject);
begin
  try
    UpdateCurrTime(mplPlayer.Position, mplPlayer.Length);
    if mplPlayer.Position
      >= mplPlayer.Length then
    begin
      if Rep then
      begin
        mplPlayer.Rewind;
        mplPlayer.Play;
        mplPlayer.Notify := True;
      end
      else
      begin
        if Loop then
        begin
     //     if NowPlay in [1..MaxFileNo] then
     //     begin
            if NowPlay + 1 > MaxNo then
            begin
              PlayFile(1)
            end
            else
              PlayFile(NowPlay+1);
      //    end;
        end
        else
          lblStopClick(Sender);
      end;
    end;
  except
    ;
  end;
end;

procedure TfraPlayer.mplPlayerNotify(Sender: TObject);
begin
      try
        if (frmMain.mplBack.Mode = mpPlaying) and (mplPlayer.Mode = mpPlaying) then
        begin
          frmMain.mplBack.Stop;
          mplBackPlay := False;
        end;
        frmMain.mplBack.Notify := True;
      except;end;
  with Sender as TMediaPlayer do
  begin
    Notify := True;
    if (Mode = mpPaused) or (Mode = mpStopped) then
    begin
      imgPause.Visible := False;
      imgPauseO.Visible := False;
    end
    else if Mode = mpPlaying then
    begin
      imgPlay.Visible := False;
      imgPause.Visible := True;
    end;
  end;
end;

end.
