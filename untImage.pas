unit untImage;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, ExtCtrls, StdCtrls, Buttons, jpeg, General;

type
  TfraImage = class(TFrame)
    imgLarge: TImage;
    imgThumbnail1: TImage;
    imgThumbnail2: TImage;
    imgThumbnail3: TImage;
    imgThumbnail4: TImage;
    imgImageBack: TImage;
    imgPreImage: TImage;
    imgNextImage: TImage;
    lblPreImage: TLabel;
    lblNextImage: TLabel;
    imgD: TImage;
    imgC: TImage;
    imgB: TImage;
    imgA: TImage;
    lblA: TLabel;
    lblB: TLabel;
    lblC: TLabel;
    lblD: TLabel;
    procedure imgThumbnail1Click(Sender: TObject);
    procedure imgThumbnail2Click(Sender: TObject);
    procedure imgThumbnail3Click(Sender: TObject);
    procedure imgThumbnail4Click(Sender: TObject);
    procedure imgLargeDblClick(Sender: TObject);
    procedure lblPreImageMouseEnter(Sender: TObject);
    procedure lblPreImageMouseLeave(Sender: TObject);
    procedure lblPreImageClick(Sender: TObject);
    procedure lblNextImageMouseEnter(Sender: TObject);
    procedure lblNextImageMouseLeave(Sender: TObject);
    procedure lblNextImageClick(Sender: TObject);
    procedure lblAMouseEnter(Sender: TObject);
    procedure lblAMouseLeave(Sender: TObject);
    procedure lblBMouseEnter(Sender: TObject);
    procedure lblBMouseLeave(Sender: TObject);
    procedure lblCMouseEnter(Sender: TObject);
    procedure lblCMouseLeave(Sender: TObject);
    procedure lblDMouseEnter(Sender: TObject);
    procedure lblDMouseLeave(Sender: TObject);
    procedure lblAClick(Sender: TObject);
    procedure lblBClick(Sender: TObject);
    procedure lblCClick(Sender: TObject);
    procedure lblDClick(Sender: TObject);
  private
    procedure LoadThumbImage(No: Byte);
    procedure LoadLargeImage(No: Byte);
    function GetPicName(No: Byte; LargeOrSmall: Byte): string;
    procedure LoadImageOrNone(MyImage: TImage; PicName: String);
    procedure SwitchImageFolder(ImageFolder: Char);
    { Private declarations }
  public
    { Public declarations }
    procedure FirstLoad;
  end;

var
  CurrentImageNo, CurrentLargeImageNo: Byte;
  CurrentImageFolder: Char;
  ImagesNum: Integer;

implementation

uses
  untMain, untFullScreenImage;

{$R *.dfm}

procedure TfraImage.FirstLoad;
begin
  CurrentLargeImageNo := 0;
  CurrentImageNo := 0;
  SwitchImageFolder('a');
//  LoadThumbImage(1);
//  LoadLargeImage(1);
end;

procedure TfraImage.SwitchImageFolder(ImageFolder: Char);
begin
  imgA.Visible := False;
  imgB.Visible := False;
  imgC.Visible := False;
  imgD.Visible := False;
  case ImageFolder of
    'a':
      begin
        imgA.Visible := True;
        CurrentImageFolder := 'a';
        ImagesNum := 20;
      end;
    'b':
      begin
        imgB.Visible := True;
        CurrentImageFolder := 'b';
        ImagesNum := 26;
      end;
    'c':
      begin
        imgC.Visible := True;
        CurrentImageFolder := 'c';
        ImagesNum := 12;
      end;
    'd':
      begin
        imgD.Visible := True;
        CurrentImageFolder := 'd';
        ImagesNum := 13;
      end;
  end;
  LoadThumbImage(0);
  LoadLargeImage(1);
end;

procedure TfraImage.LoadThumbImage(No: Byte);
begin
  if No = 0 then
    CurrentImageNo := 0;
  if No < 1 then
    No := 1
  else if No > (ImagesNum - 3) then
    No := ImagesNum - 3;
  if CurrentImageNo <> No then
  begin
    CurrentImageNo := No;
    LoadImageOrNone(imgThumbnail1, GetPicName(CurrentImageNo, 2));
    LoadImageOrNone(imgThumbnail2, GetPicName(CurrentImageNo + 1, 2));
    LoadImageOrNone(imgThumbnail3, GetPicName(CurrentImageNo + 2, 2));
    LoadImageOrNone(imgThumbnail4, GetPicName(CurrentImageNo + 3, 2));
  end;
end;

function TfraImage.GetPicName(No: Byte; LargeOrSmall: Byte): string;
var
  LS: String[1];
begin
  if LargeOrSmall = 1 then
    LS := ''
  else if LargeOrSmall = 2 then
    LS := 's';
  Result := ImagePath + CurrentImageFolder + '\' + IntToStr(No) + LS + '.jpg';
end;

procedure TfraImage.LoadImageOrNone(MyImage: TImage; PicName: String);
begin
  if not FileExists(PicName) then
    PicName := ImagePath + 'none.bmp';
  MyImage.Picture.LoadFromFile(PicName);
end;

procedure TfraImage.LoadLargeImage(No: Byte);
begin
//  if No :=
//  if CurrentLargeImageNo <> No then
//  begin
    LoadImageOrNone(imgLarge, GetPicName(No, 1));
    CurrentLargeImageNo := No;
//  end;
end;

procedure TfraImage.imgThumbnail1Click(Sender: TObject);
begin
  LoadLargeImage(CurrentImageNo);
end;

procedure TfraImage.imgThumbnail2Click(Sender: TObject);
begin
  LoadLargeImage(CurrentImageNo + 1);
end;

procedure TfraImage.imgThumbnail3Click(Sender: TObject);
begin
  LoadLargeImage(CurrentImageNo + 2);
end;

procedure TfraImage.imgThumbnail4Click(Sender: TObject);
begin
  LoadLargeImage(CurrentImageNo + 3);
end;

procedure TfraImage.imgLargeDblClick(Sender: TObject);
begin
  frmFullScreenImage := TfrmFullScreenImage.Create(Self);
  LoadImageOrNone(frmFullScreenImage.imgFullScreenImage,
    GetPicName(CurrentLargeImageNo, 1));
  frmFullScreenImage.ShowModal;
end;

procedure TfraImage.lblPreImageMouseEnter(Sender: TObject);
begin
  imgPreImage.Visible := True;
end;

procedure TfraImage.lblPreImageMouseLeave(Sender: TObject);
begin
  imgPreImage.Visible := False;
end;

procedure TfraImage.lblPreImageClick(Sender: TObject);
begin
  LoadThumbImage(CurrentImageNo - 1);
end;

procedure TfraImage.lblNextImageMouseEnter(Sender: TObject);
begin
  imgNextImage.Visible := True;
end;

procedure TfraImage.lblNextImageMouseLeave(Sender: TObject);
begin
  imgNextImage.Visible := False;
end;

procedure TfraImage.lblNextImageClick(Sender: TObject);
begin
  LoadThumbImage(CurrentImageNo + 1);
end;

procedure TfraImage.lblAMouseEnter(Sender: TObject);
begin
  imgA.Visible := True;
end;

procedure TfraImage.lblAMouseLeave(Sender: TObject);
begin
  if not (CurrentImageFolder = 'a') then
    imgA.Visible := False;
end;

procedure TfraImage.lblBMouseEnter(Sender: TObject);
begin
  imgB.Visible := True;
end;

procedure TfraImage.lblBMouseLeave(Sender: TObject);
begin
  if not (CurrentImageFolder = 'b') then
    imgB.Visible := False;
end;

procedure TfraImage.lblCMouseEnter(Sender: TObject);
begin
  imgC.Visible := True;
end;

procedure TfraImage.lblCMouseLeave(Sender: TObject);
begin
  if not (CurrentImageFolder = 'c') then
    imgC.Visible := False;
end;

procedure TfraImage.lblDMouseEnter(Sender: TObject);
begin
  imgD.Visible := True;
end;

procedure TfraImage.lblDMouseLeave(Sender: TObject);
begin
  if not (CurrentImageFolder = 'd') then
    imgD.Visible := False;
end;

procedure TfraImage.lblAClick(Sender: TObject);
begin
  SwitchImageFolder('a');
end;

procedure TfraImage.lblBClick(Sender: TObject);
begin
  SwitchImageFolder('b');
end;

procedure TfraImage.lblCClick(Sender: TObject);
begin
  SwitchImageFolder('c');
end;

procedure TfraImage.lblDClick(Sender: TObject);
begin
  SwitchImageFolder('d');
end;

end.
