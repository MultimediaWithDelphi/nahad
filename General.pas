unit General;

interface

uses
  IniFiles, Dialogs, Forms, ExtCtrls, StdCtrls, Controls;

const
  ALPHABLEND_DIFF = 50;
  DATA_FOLDER = 'data\';
  IMAGES_FOLDER = 'images\';

  VIDEO_FOLDER = 'video\';
  AUDIO_FOLDER = 'audio\';
  IMAGE_FOLDER = 'image\';
  CLIP_FOLDER = 'clip\';
  TEXT_FOLDER = 'text\';

var
  configuration: TIniFile;
  VideoPath, AudioPath, DataPath, ClipPath,
  TextPathA, TextPathB, ImagePath,
  ImagesPath: String;
  // ImagePath is for images of Image Frame
  // but ImagesPath is for images of all forms
  // that must be loaded at the begining of
  // application run.

procedure ReadIni_Player_Audio(Section: String);
procedure ReadIni_Player_Clip(Section: String);
procedure ReadIni_Player_Video(Section: String);
procedure ReadOrCreateIni(Section: String);
function IniRead(Section, Ident, Default: String): String; overload
function IniRead(Section, Ident :String; Default: Integer): Integer; overload
procedure ObjectSet(Control1: TControl; Section :String; H, L, T, W: Integer);
function MyDir(S: String): String;
procedure PreparePathNames;
procedure ImageLoad(Image1: TImage; ImageFileName: String);

implementation

uses
  untBesm, untSplash, untMain, untAudio, untPlayer, SysUtils;

function IniRead(Section, Ident, Default: String): String;
var
  s: String;
begin
  if configuration.ValueExists(Section, Ident) then
    s := PChar(configuration.ReadString(Section, Ident, Default))
  else
  begin
    configuration.WriteString(Section, Ident, Default);
    s := Default;
  end;
  Result := s;
end;

function IniRead(Section, Ident :String; Default: Integer): Integer;
var
  i: Integer;
begin
  if configuration.ValueExists(Section, Ident) then
    i := configuration.ReadInteger(Section, Ident, Default)
  else
  begin
    configuration.WriteInteger(Section, Ident, Default);
    i := Default;
  end;
  Result := i;
end;

procedure ObjectSet(Control1: TControl; Section :String; H, L, T, W: Integer);
begin
  with Control1 do
  begin
    Height := IniRead(Section, Control1.Name + '.Height', H);
    Left := IniRead(Section, Control1.Name + '.Left', L);
    Top := IniRead(Section, Control1.Name + '.Top', T);
    Width := IniRead(Section, Control1.Name + '.Width', W);
  end;
{  if not (Label1 = nil) then
  begin
    with Label1 do
    begin
      Height := IniRead(Section, Label1.Name + '.Height', H);
      Left := IniRead(Section, Label1.Name + '.Left', L);
      Top := IniRead(Section, Label1.Name + '.Top', T);
      Width := IniRead(Section, Label1.Name + '.Width', W);
    end;
  end;}
end;

procedure ReadOrCreateIni(Section: String);
begin
  configuration := TIniFile.Create( DATA_FOLDER + 'configuration.nahad' );
  if configuration <> nil then
  begin

    ////   0
    if Section = 'Application' then
    begin
      Application.Title := IniRead(Section, 'Title', '���� �����ϐ� ���� ���� �����');
    end
    else

    ////   1
    if Section = 'Besm' then
    begin
      frmBesm.Timer1.Interval := IniRead(Section, 'TimerInterval', 2000);
    end
    else

    ////   2
    if Section = 'Splash' then
    begin
      frmSplash.Timer1.Interval := IniRead(Section, 'TimerInterval', 2000);
    end
    else

    ////   3
    if Section = 'Main' then
    begin
      with frmMain do
      begin
        ObjectSet(imgAudio, Section, 35, 373, 627, 63);
        ObjectSet(lblAudio, Section, 35, 373, 627, 63);
        ObjectSet(imgClip, Section, 35, 572, 623, 68);
        ObjectSet(lblClip, Section, 35, 572, 623, 68);
        ObjectSet(imgExit, Section, 26, 172, 624, 27);
        ObjectSet(lblExit, Section, 26, 172, 624, 27);
        ObjectSet(imgImage, Section, 41, 311, 613, 59);
        ObjectSet(lblImage, Section, 41, 311, 613, 59);
        ObjectSet(imgMin, Section, 26, 200, 624, 27);
        ObjectSet(lblMin, Section, 26, 200, 624, 27);
        ObjectSet(imgMusic, Section, 26, 257, 624, 27);
        ObjectSet(lblMusic, Section, 26, 257, 624, 27);
        ObjectSet(imgSysTray, Section, 26, 228, 624, 27);
        ObjectSet(lblSysTray, Section, 26, 228, 624, 27);
        ObjectSet(imgText, Section, 27, 503, 628, 62);
        ObjectSet(lblText, Section, 27, 503, 628, 62);
        ObjectSet(imgVideo, Section, 32, 437, 621, 60);
        ObjectSet(lblVideo, Section, 32, 437, 621, 60);

        ///// settings for frames...
        ObjectSet(fraAudio1, Section, 425, 303, 186, 553);
        ObjectSet(fraClip1, Section, 425, 303, 186, 553);
        ObjectSet(fraImage1, Section, 484, 303, 127, 553);
        ObjectSet(fraText1, Section, 450, 312, 128, 540);
        ObjectSet(fraVideo1, Section, 425, 303, 186, 553);
      end;
    end
    else

    ////   4
    if Section = 'Audio' then
    begin
      with frmMain.fraAudio1 do
      begin
        ObjectSet(fraBtns1, Section, 267, 5, 117, 146);     /////||||\\\\\
        ObjectSet(fraPlayer1, Section, 75, 191, 48, 351);
        ObjectSet(pnlScreen, Section, 240, 186, 144, 320);
        ObjectSet(imgDisplayPanel, Section, 236, 0, 0, 316);
        tmrImage.Interval := IniRead(Section, 'TimerInterval', 5000);
      end;
      ReadIni_Player_Audio('Player');
    end
    else

    ////   5
    if Section = 'Btns' then
    begin
      ;/////|||\\\\
    end
    else

    ////   6
    if Section = 'Clip' then
    begin
      with frmMain.fraClip1 do
      begin
        ObjectSet(fraPlayer1, Section, 75, 191, 48, 351);
        ObjectSet(pnlScreen, Section, 240, 186, 144, 320);
        ObjectSet(imgDisplayPanel, Section, 240, 0, 0, 316);
      end;
      ReadIni_Player_Clip('Player');
    end
    else

    ////   7
    if Section = 'Video' then
    begin
      with frmMain.fraVideo1 do
      begin
        ObjectSet(fraBtns1, Section, 267, 5, 117, 146);     /////||||\\\\\
        ObjectSet(fraPlayer1, Section, 75, 191, 48, 351);
        ObjectSet(pnlScreen, Section, 240, 186, 144, 320);
        ObjectSet(imgDisplayPanel, Section, 236, 0, 0, 316);
      end;
      ReadIni_Player_Video('Player');
    end
    else

    ////   8
    if Section = 'Text' then
    begin
      ;/////||||\\\\\\\\\
    end
    else

    ////   9
    if Section = 'Image' then
    begin
      with frmMain.fraImage1 do
      begin
        ObjectSet(imgLarge, Section, 292, 80, 50, 393);
        ObjectSet(imgNextImage, Section, 22, 509, 395, 36);
        ObjectSet(imgPreImage, Section, 21, 6, 396, 34);
        ObjectSet(imgThumbnail1, Section, 78, 42, 364, 105);
        ObjectSet(imgThumbnail2, Section, 78, 161, 364, 105);
        ObjectSet(imgThumbnail3, Section, 78, 282, 364, 105);
        ObjectSet(imgThumbnail4, Section, 78, 400, 364, 105);
      end;
    end;

    configuration.Free;
  end
  else
  begin
    ShowMessage('Error in reading/creating configuration INI file.');
    //Application.Terminate;
  end;
end;

procedure ReadIni_Player_Audio(Section: String);
begin
  with frmMain.fraAudio1.fraPlayer1 do
  begin
    ObjectSet(imgForward, Section, 21, 136, 41, 22);
    ObjectSet(imgFwD, Section, 21, 136, 41, 22);
    ObjectSet(lblForward, Section, 21, 136, 41, 22);
    ObjectSet(imgLoop, Section, 21, 194, 53, 22);
    ObjectSet(imgLoopD, Section, 21, 194, 53, 22);
    ObjectSet(lblLoop, Section, 21, 194, 53, 22);
    ObjectSet(imgMute, Section, 19, 276, 41, 19);
    ObjectSet(imgMuteD, Section, 19, 276, 41, 19);
    ObjectSet(lblMute, Section, 19, 276, 41, 19);
    ObjectSet(imgNext, Section, 21, 165, 41, 21);
    ObjectSet(imgNextD, Section, 21, 165, 41, 21);
    ObjectSet(lblNext, Section, 21, 165, 41, 21);
    ObjectSet(imgPause, Section, 32, 64, 35, 34);
    ObjectSet(imgPauseD, Section, 32, 64, 35, 34);
    ObjectSet(imgPauseO, Section, 32, 64, 35, 34);
    ObjectSet(imgPlay, Section, 32, 64, 35, 34);
    ObjectSet(imgPlayD, Section, 32, 64, 35, 34);
    ObjectSet(lblPlayPause, Section, 32, 64, 35, 34);
    ObjectSet(imgPre, Section, 21, 5, 41, 21);
    ObjectSet(imgPreD, Section, 21, 5, 41, 21);
    ObjectSet(lblPre, Section, 21, 5, 41, 21);
    lblPre.BringToFront;
    ObjectSet(imgRepeat, Section, 21, 194, 27, 22);
    ObjectSet(imgRepD, Section, 21, 194, 27, 22);
    ObjectSet(lblRepeat, Section, 21, 194, 27, 22);
    ObjectSet(imgRewind, Section, 21, 32, 41, 22);
    ObjectSet(imgRwD, Section, 21, 32, 41, 22);
    ObjectSet(lblRewind, Section, 21, 32, 41, 22);
    ObjectSet(imgSound, Section, 9, 297, 47, 49);
    ObjectSet(lblChangeSound, Section, 9, 297, 47, 49);
    ObjectSet(imgStop, Section, 25, 104, 39, 26);
    ObjectSet(imgStopD, Section, 25, 104, 39, 26);
    ObjectSet(lblStop, Section, 25, 104, 39, 26);
    ObjectSet(imgTrackMove, Section, 26, 1, 1, 347);
    ObjectSet(lblTrackUser, Section, 26, 1, 1, 347);
    ObjectSet(lblFullTime, Section, 17, 222, 51, 41);
    ObjectSet(lblTime, Section, 17, 222, 32, 41);
  end;
end;

procedure ReadIni_Player_Clip(Section: String);
begin
  with frmMain.fraClip1.fraPlayer1 do
  begin
    ObjectSet(imgForward, Section, 21, 136, 41, 22);
    ObjectSet(imgFwD, Section, 21, 136, 41, 22);
    ObjectSet(lblForward, Section, 21, 136, 41, 22);
    ObjectSet(imgLoop, Section, 21, 194, 53, 22);
    ObjectSet(imgLoopD, Section, 21, 194, 53, 22);
    ObjectSet(lblLoop, Section, 21, 194, 53, 22);
    ObjectSet(imgMute, Section, 19, 276, 41, 19);
    ObjectSet(imgMuteD, Section, 19, 276, 41, 19);
    ObjectSet(lblMute, Section, 19, 276, 41, 19);
    ObjectSet(imgNext, Section, 21, 165, 41, 21);
    ObjectSet(imgNextD, Section, 21, 165, 41, 21);
    ObjectSet(lblNext, Section, 21, 165, 41, 21);
    ObjectSet(imgPause, Section, 32, 64, 35, 34);
    ObjectSet(imgPauseD, Section, 32, 64, 35, 34);
    ObjectSet(imgPauseO, Section, 32, 64, 35, 34);
    ObjectSet(imgPlay, Section, 32, 64, 35, 34);
    ObjectSet(imgPlayD, Section, 32, 64, 35, 34);
    ObjectSet(lblPlayPause, Section, 32, 64, 35, 34);
    ObjectSet(imgPre, Section, 21, 5, 41, 21);
    ObjectSet(imgPreD, Section, 21, 5, 41, 21);
    ObjectSet(lblPre, Section, 21, 5, 41, 21);
    ObjectSet(imgRepeat, Section, 21, 194, 27, 22);
    ObjectSet(imgRepD, Section, 21, 194, 27, 22);
    ObjectSet(lblRepeat, Section, 21, 194, 27, 22);
    ObjectSet(imgRewind, Section, 21, 32, 41, 22);
    ObjectSet(imgRwD, Section, 21, 32, 41, 22);
    ObjectSet(lblRewind, Section, 21, 32, 41, 22);
    ObjectSet(imgSound, Section, 9, 297, 47, 49);
    ObjectSet(lblChangeSound, Section, 9, 297, 47, 49);
    ObjectSet(imgStop, Section, 25, 104, 39, 26);
    ObjectSet(imgStopD, Section, 25, 104, 39, 26);
    ObjectSet(lblStop, Section, 25, 104, 39, 26);
    ObjectSet(imgTrackMove, Section, 26, 1, 1, 347);
    ObjectSet(lblTrackUser, Section, 26, 1, 1, 347);
    ObjectSet(lblFullTime, Section, 17, 222, 51, 41);
    ObjectSet(lblTime, Section, 17, 222, 32, 41);
  end;
end;

procedure ReadIni_Player_Video(Section: String);
begin
  with frmMain.fraVideo1.fraPlayer1 do
  begin
    ObjectSet(imgForward, Section, 21, 136, 41, 22);
    ObjectSet(imgFwD, Section, 21, 136, 41, 22);
    ObjectSet(lblForward, Section, 21, 136, 41, 22);
    ObjectSet(imgLoop, Section, 21, 194, 53, 22);
    ObjectSet(imgLoopD, Section, 21, 194, 53, 22);
    ObjectSet(lblLoop, Section, 21, 194, 53, 22);
    ObjectSet(imgMute, Section, 19, 276, 41, 19);
    ObjectSet(imgMuteD, Section, 19, 276, 41, 19);
    ObjectSet(lblMute, Section, 19, 276, 41, 19);
    ObjectSet(imgNext, Section, 21, 165, 41, 21);
    ObjectSet(imgNextD, Section, 21, 165, 41, 21);
    ObjectSet(lblNext, Section, 21, 165, 41, 21);
    ObjectSet(imgPause, Section, 32, 64, 35, 34);
    ObjectSet(imgPauseD, Section, 32, 64, 35, 34);
    ObjectSet(imgPauseO, Section, 32, 64, 35, 34);
    ObjectSet(imgPlay, Section, 32, 64, 35, 34);
    ObjectSet(imgPlayD, Section, 32, 64, 35, 34);
    ObjectSet(lblPlayPause, Section, 32, 64, 35, 34);
    ObjectSet(imgPre, Section, 21, 5, 41, 21);
    ObjectSet(imgPreD, Section, 21, 5, 41, 21);
    ObjectSet(lblPre, Section, 21, 5, 41, 21);
    ObjectSet(imgRepeat, Section, 21, 194, 27, 22);
    ObjectSet(imgRepD, Section, 21, 194, 27, 22);
    ObjectSet(lblRepeat, Section, 21, 194, 27, 22);
    ObjectSet(imgRewind, Section, 21, 32, 41, 22);
    ObjectSet(imgRwD, Section, 21, 32, 41, 22);
    ObjectSet(lblRewind, Section, 21, 32, 41, 22);
    ObjectSet(imgSound, Section, 9, 297, 47, 49);
    ObjectSet(lblChangeSound, Section, 9, 297, 47, 49);
    ObjectSet(imgStop, Section, 25, 104, 39, 26);
    ObjectSet(imgStopD, Section, 25, 104, 39, 26);
    ObjectSet(lblStop, Section, 25, 104, 39, 26);
    ObjectSet(imgTrackMove, Section, 26, 1, 1, 347);
    ObjectSet(lblTrackUser, Section, 26, 1, 1, 347);
    ObjectSet(lblFullTime, Section, 17, 222, 51, 41);
    ObjectSet(lblTime, Section, 17, 222, 32, 41);
  end;
end;

function MyDir(S: String): String;
begin
  S := GetCurrentDir + '\' + S;
  while Pos('\\', S) > 0 do
    Delete(S, Pos('\\', S), 1);
  Result := S;
end;

procedure PreparePathNames;
begin
  VideoPath := MyDir(VIDEO_FOLDER);
  AudioPath := MyDir(AUDIO_FOLDER);
  DataPath := MyDir(DATA_FOLDER);
  ClipPath := MyDir(CLIP_FOLDER);
  TextPathA := MyDir(TEXT_FOLDER + 'a\');
  TextPathB := MyDir(TEXT_FOLDER + 'b\');
  ImagePath := MyDir(IMAGE_FOLDER);
  ImagesPath := MyDir(IMAGES_FOLDER)
end;

procedure ImageLoad(Image1: TImage; ImageFileName: String);
var
  fullFileName: String;
begin
  fullFileName := ImagesPath + ImageFileName;
//  ShowMessage(fullFileName);
  if FileExists(fullFileName) then
    Image1.Picture.LoadFromFile(fullFileName);
end;

initialization
begin
  PreparePathNames;
end;

end.
