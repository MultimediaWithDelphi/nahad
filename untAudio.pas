unit untAudio;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, StdCtrls, MPlayer, jpeg, ExtCtrls, untPlayer, untBtns, ActnList,
  General;

type
  TfraAudio = class(TFrame)
    Image1: TImage;
    pnlScreen: TPanel;
    imgDisplayPanel: TImage;
    fraBtns1: TfraBtns;
    fraPlayer1: TfraPlayer;
    tmrImage: TTimer;
    procedure imgDisplayPanelDblClick(Sender: TObject);
    procedure fraBtns1lbl1Click(Sender: TObject);
    procedure fraBtns1lbl2Click(Sender: TObject);
    procedure fraBtns1lbl3Click(Sender: TObject);
    procedure fraBtns1lbl4Click(Sender: TObject);
    procedure fraBtns1lbl5Click(Sender: TObject);
    procedure tmrImageTimer(Sender: TObject);
    procedure fraPlayer1lblPlayPauseClick(Sender: TObject);
    procedure fraPlayer1lblStopClick(Sender: TObject);
  private
    procedure LoadLargeImage(No: Byte);
    procedure SwitchImageFolder(ImageFolder: Char);
    { Private declarations }
  public
    { Public declarations }
    procedure LoadImageOrNone(MyImage: TImage; PicName: String);
    function GetPicName(No, LargeOrSmall: Byte): string;
  end;

var
  CurrentLargeImageNo: Byte;
  CurrentImageFolder: Char;
  ImagesNum: Integer;
  FullScreen: Boolean;

implementation

uses
  untMain, untFullScreenImage;

{$R *.dfm}

procedure TfraAudio.SwitchImageFolder(ImageFolder: Char);
begin
  case ImageFolder of
    'a':
      begin
        CurrentImageFolder := 'a';
        ImagesNum := 20;
      end;
    'b':
      begin
        CurrentImageFolder := 'b';
        ImagesNum := 26;
      end;
    'c':
      begin
        CurrentImageFolder := 'c';
        ImagesNum := 12;
      end;
    'd':
      begin
        CurrentImageFolder := 'd';
        ImagesNum := 13;
      end;
  end;
  LoadLargeImage(1);
  tmrImage.Enabled := True;
end;

function TfraAudio.GetPicName(No: Byte; LargeOrSmall: Byte): string;
var
  LS: String[1];
begin
  if LargeOrSmall = 1 then
    LS := ''
  else if LargeOrSmall = 2 then
    LS := 's';
  Result := ImagePath + CurrentImageFolder + '\' + IntToStr(No) + LS + '.jpg';
end;

procedure TfraAudio.LoadImageOrNone(MyImage: TImage; PicName: String);
begin
  if not FileExists(PicName) then
    PicName := ImagePath + 'none.bmp';
  MyImage.Picture.LoadFromFile(PicName);
end;

procedure TfraAudio.LoadLargeImage(No: Byte);
begin
  imgDisplayPanel.Proportional := True;
  LoadImageOrNone(imgDisplayPanel, GetPicName(No, 1));
  CurrentLargeImageNo := No;
end;

procedure TfraAudio.imgDisplayPanelDblClick(Sender: TObject);
begin
  FullScreen := True;
  frmFullScreenImage := TfrmFullScreenImage.Create(Self);
  LoadImageOrNone(frmFullScreenImage.imgFullScreenImage,
    GetPicName(CurrentLargeImageNo, 1));
  frmFullScreenImage.ShowModal;
end;

procedure TfraAudio.fraBtns1lbl1Click(Sender: TObject);
begin
  fraBtns1.lbl1Click(Sender);
  SwitchImageFolder('a');
end;

procedure TfraAudio.fraBtns1lbl2Click(Sender: TObject);
begin
  fraBtns1.lbl2Click(Sender);
  SwitchImageFolder('c');
end;

procedure TfraAudio.fraBtns1lbl3Click(Sender: TObject);
begin
  fraBtns1.lbl3Click(Sender);
  SwitchImageFolder('b');
end;

procedure TfraAudio.fraBtns1lbl4Click(Sender: TObject);
begin
  fraBtns1.lbl4Click(Sender);
  SwitchImageFolder('b');
end;

procedure TfraAudio.fraBtns1lbl5Click(Sender: TObject);
begin
  fraBtns1.lbl5Click(Sender);
  SwitchImageFolder('d');
end;

procedure TfraAudio.tmrImageTimer(Sender: TObject);
begin
  Inc(CurrentLargeImageNo);
  if CurrentLargeImageNo > ImagesNum then
    CurrentLargeImageNo := 1;
  if FullScreen then
    LoadImageOrNone(frmFullScreenImage.imgFullScreenImage,
      GetPicName(CurrentLargeImageNo, 1));

  LoadLargeImage(CurrentLargeImageNo);
end;

procedure TfraAudio.fraPlayer1lblPlayPauseClick(Sender: TObject);
begin
  fraPlayer1.lblPlayPauseClick(Sender);
  if (fraPlayer1.mplPlayer.Mode = mpPlaying) then
    tmrImage.Enabled := True
  else
    tmrImage.Enabled := False;
end;

procedure TfraAudio.fraPlayer1lblStopClick(Sender: TObject);
begin
  fraPlayer1.lblStopClick(Sender);
  tmrImage.Enabled := False;
end;

end.
